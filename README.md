1. Multiple 'Git Pull' with for loop - Bash
``for d in <path-to-repos>/*; do (cd $d && git pull); done``

2. Multiple 'Git Pull' with find - Bash
``find . -maxdepth 1 -type d \( ! -name . \) -exec bash -c "cd '{}' && git pull" \;``
